﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace FileMerger
{
    public partial class FileMerger : Form
    {
        private delegate void OpenFileDelegate();
        private delegate void UpdateUIDelegate();
        OpenFileDelegate openFileDelegate;
        UpdateUIDelegate updateUIDelegate;

        #region HTML Tags
        private string tagHome = "<!DOCTYPE html><html><head><meta http-equiv=\"Content-Type\" content=\"text/html\"; charset=\"utf-8\"></head><body><p>";
        private string tagLFCR = "<br/>";
        private string tagEnd = "</p></body></html>";
        private string tagSelectHomeGreen = "<div style=\"background-color:palegreen\">";
        private string tagSelectHomeRed = "<div style=\"background-color:pink\">";
        private string tagSelectHomeYellow = "<div style=\"background-color:lemonchiffon\">";
        private string tagSelectEnd = "</div>";
        #endregion
         
        private string lineBegin;
        private string lineEdit1;
        private string lineEdit2;
        private string lineResult;

        private OpenFileDialog openFileDialog;
        private StreamReader fileBegin;
        private StreamReader fileEdit1;
        private StreamReader fileEdit2;
        private bool filesChoose;

        public FileMerger()
        {
            InitializeComponent();
            openFileDelegate = new OpenFileDelegate(OpenFiles);
            updateUIDelegate = new UpdateUIDelegate(UpdateData);
        }

        private void OpenFiles()
        {
            #region File Logic
            filesChoose = true;
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*";
            openFileDialog.Title = "Выберите оригинальный файл";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                fileBegin = new StreamReader(openFileDialog.FileName, Encoding.GetEncoding("windows-1251"));
            else
                filesChoose = false;

            openFileDialog.Title = "Выберите измененный файл 1";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                fileEdit1 = new StreamReader(openFileDialog.FileName, Encoding.GetEncoding("windows-1251"));
            else
                filesChoose = false;

            openFileDialog.Title = "Выберите измененный файл 2";

            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                fileEdit2 = new StreamReader(openFileDialog.FileName, Encoding.GetEncoding("windows-1251"));
            else
                filesChoose = false;
            DialogResult result;
            if (!filesChoose)
            {
                result = MessageBox.Show("Не все файлы были выбраны. Невозможно выполнить слияние", "Ошибка", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);

                if (result == DialogResult.Retry)
                {
                    OpenFiles();
                }
            }
            #endregion
        }

        private void UpdateData()
        {
            #region Line Logic
            if (filesChoose)
            {
                string webBrowserStr = tagHome;

                while ((lineBegin = fileBegin.ReadLine()) != null &&
                    (lineEdit1 = fileEdit1.ReadLine()) != null &&
                    (lineEdit2 = fileEdit2.ReadLine()) != null)
                {
                    bool lineEdit1Equals = lineEdit1.Equals(lineBegin);
                    bool lineEdit2Equals = lineEdit2.Equals(lineBegin);

                    if (lineEdit1Equals && lineEdit2Equals)
                        lineResult = tagSelectHomeGreen + lineBegin + tagSelectEnd;
                    else if (!lineEdit1Equals && !lineEdit2Equals)
                        lineResult = tagSelectHomeRed + "<------" + tagLFCR + lineEdit1 +
                            tagLFCR + "------" + tagLFCR + lineEdit2 + tagLFCR + "------>" + tagSelectEnd;
                    else
                    {
                        if (!lineEdit1Equals && lineEdit2Equals)
                            lineResult = tagSelectHomeYellow + lineEdit1 + tagSelectEnd;
                        else
                            lineResult = tagSelectHomeYellow + lineEdit2 + tagSelectEnd;
                    }
                    webBrowserStr += lineResult;
                }
                webBrowserStr += tagEnd;
                webBrowserPage.DocumentText = webBrowserStr;
                label1.Visible = false;
            }
            #endregion
        }

        private void OpenMenu_Click(object sender, EventArgs e)
        {
            this.Invoke(openFileDelegate);
            this.Invoke(updateUIDelegate);
        }

        private void AboutMenu_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Программа построчного слияния файлов\n\nАвтор: Резников Ю.Е.\nEmail: reznikov_yura@mail.ru", "О программе", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
